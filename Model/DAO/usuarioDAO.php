<?php

require_once '../Model/Conexao/connection.php';

class usuarioDAO{
  
    private $conn;
    private $cod_usuario;
    private $nome;
    private $sobrenome;
    private $username;
    private $email;
    private $senha;
    private $nivel;
    private $data_nascimento;
    private $idade;
    private $morada;
    private $sexo;
    private $telefone;
  
    
    function getCod_usuario() {
        return $this->cod_usuario;
    }

    function getNome() {
        return $this->nome;
    }

    function getSobrenome() {
        return $this->sobrenome;
    }

    function getUsername() {
        return $this->username;
    }

    function getEmail() {
        return $this->email;
    }

    function getSenha() {
        return $this->senha;
    }

    function getNivel() {
        return $this->nivel;
    }

    function getData_nascimento() {
        return $this->data_nascimento;
    }

    function getIdade() {
        return $this->idade;
    }

    function getMorada() {
        return $this->morada;
    }

    function getSexo() {
        return $this->sexo;
    }

    function getTelefone() {
        return $this->telefone;
    }

    function setCod_usuario($cod_usuario) {
        $this->cod_usuario = $cod_usuario;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setSobrenome($sobrenome) {
        $this->sobrenome = $sobrenome;
    }

    function setUsername($username) {
        $this->username = $username;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setSenha($senha) {
        $this->senha = $senha;
    }

    function setNivel($nivel) {
        $this->nivel = $nivel;
    }

    function setData_nascimento($data_nascimento) {
        $this->data_nascimento = $data_nascimento;
    }

    function setIdade($idade) {
        $this->idade = $idade;
    }

    function setMorada($morada) {
        $this->morada = $morada;
    }

    function setSexo($sexo) {
        $this->sexo = $sexo;
    }

    function setTelefone($telefone) {
        $this->telefone = $telefone;
    }
    
 public function usuarioDAO() {
        $this->conn = getConnection();
    }
    
    
    public function inserir() {
        $inseri=$this->conn->prepare("INSERT INTO usuario(nome,sobrenome,username,email,senha,idade,sexo,telefone,data_nascimento,morada,nivel )"
                . "VALUES('$this->nome','$this->sobrenome','$this->username','$this->email','$this->senha','$this->idade','$this->sexo','$this->telefone','$this->data_nascimento','$this->morada','$this->nivel') ");
        $inseri->execute();
         if($inseri):
            echo '<script>alert("Funcionario Registado Com Sucesso!!")</script>';
        else:
            echo '<script>alert("Erro ao Registar o Funcionario!!")</script>';
        endif;
        
        return $inseri;
        
    }


    public function delete() {
        
    }

    public function update() {
        
    }

}

        
    
    
    
