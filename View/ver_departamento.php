<?php

session_start();
require_once '../Model/Conexao/connection.php';

$pdo=getConnection();

 $consulta=$pdo->prepare("SELECT  * FROM  departamento");
  $consulta->execute();
  $msg_contatos=$consulta->fetchAll(PDO::FETCH_ASSOC);

	   
	
	
?>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width ,initial-scale=1, shirink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">


<link href="../content/node_modules/bootstrap-4.0.0/dist/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="../content/css/style.css" rel="stylesheet" type="text/css"/>
<link href="../content/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

<title>SGA</title>
</head>

<body>
    <header>
    <nav class="navbar navbar-expand -md navbar-dark fixed-top bg-dark p-1 ">
        <a href="#" data-target="#sidebar" data-toggle="collapse" aria-expanded="false"><i class="fa fa-navicon fa-2x py-2 p-1"></i></a>   
        <a class=" text-left navbar-brand col-sm-3 col-md-2 mr-0" href="#">Sistema de Gestão Academica</a> 
        </nav>
        </br>
<div class="container-fluid ">
    <div class="row d-flex d-md-block flex-nowrap wrapper">
         <div class="col-md-4 float-left col-1 pl-0 pr-0 collapse width " id="sidebar">
            </br>
               <div class="list-group border-0 card text-center text-md-left">
                
                    <a href="departamento.php" class="list-group-item d-inline-block collapsed" data-parent="#sidebar"><i class="fa fa-dashcube"></i> <span class="d-none d-md-inline">Departamento</span></a>
                    <a href="curso.php" class="list-group-item d-inline-block collapsed" data-parent="#sidebar"><i class="fa fa-list"></i> <span class="d-none d-md-inline">Curso</span></a>
                    <a href="disciplina.php" class="list-group-item d-inline-block collapsed" data-parent="#sidebar"><i class="fa fa-clock-o"></i> <span class="d-none d-md-inline">Disciplina</span></a>
                <a href="#" class="list-group-item d-inline-block collapsed" data-parent="#sidebar"><i class="fa fa-th"></i> <span class="d-none d-md-inline">Plano Curricular</span></a>
               <a href="#menu1" class="list-group-item d-inline-block collapsed" data-toggle="collapse" data-parent="#sidebar" aria-expanded="false"><i class="fa fa-gear"></i> <span class="d-none d-md-inline">Visualizar</span> </a>
                <div class="collapse" id="menu1">
                    <a href="ver_departamento.php" class="list-group-item" data-parent="#menu1">Departamento</a>
                    <a href="ver_curso.php" class="list-group-item" data-parent="#menu1">Curso</a>
                    <a href="ver_disciplina.php" class="list-group-item" data-parent="#menu1">Disciplina</a>
                     <a href="ver_usuarios.php" class="list-group-item" data-parent="#menu1">Usuarios</a>
                    
                </div>
                <a href="#" class="list-group-item d-inline-block collapsed" data-parent="#sidebar"><i class="fa fa-calendar"></i> <span class="d-none d-md-inline">Matricula</span></a>  
            </div>
        </div>
    </header>
    
    </br> </br></br> </br>
        
            
            <div class="row">
                
                <div class="col-12 text-center mb-5">
                    <h1 class="display-4"><i class=" fa fa-paper-plane text-dark" aria-hidden="true"></i>Departamentos </h1>
                        </div>
                
           
                
                <div class="col-md-8">
					<table class="table">
						<thead>
							<tr>
								<th>ID</th>
								<th>Nome Departamento</th>
                                                                
							</tr>
						</thead>
						<tbody>
							<?php 
								//Para obter os dados pode ser utilizado um while percorrendo assim cada linha retornada do banco de dados:
								foreach ($msg_contatos as $a): ?>
									<tr>
										<td><?php echo $a['cod_departamento']; ?></td>
                                                                                <td><?php echo $a['nome_departamento']; ?></td>
										
										
                                                                            
										
									</tr>    
								<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
                
            </div>
                
                
            
    <script src="../content/node_modules/jquery/dist/jquery.slim.min.js" type="text/javascript"></script>
    <script src="../content/node_modules/popper.js/dist/popper.min.js" type="text/javascript"></script>
    <script src="../content/node_modules/bootstrap-4.0.0/dist/js/bootstrap.min.js" type="text/javascript"></script>

</body>
</html>

